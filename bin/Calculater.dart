import 'dart:io';
import 'dart:math';

void main(List<String> agruments) {
  String s = stdin.readLineSync()!;
  List<String> token = tokenizerString(s);
  List<dynamic> postfix = InfixToPostfix(token);
  num result = evaluatePostfix(postfix) as num;
  print("Tokenizer: $token");
  print("Convert infix to postfix: $postfix");
  print("Evalute Postfix: $result");
}

List<String> tokenizerString(String X) {
  String token = "";
  List<String> list = [];
  for (var i = 0; i < X.length; i++) {
    if (X[i] == " ") {
      if (token.isNotEmpty) {
        list.add(token);
        token = '';
      }
    } else {
      token += X[i];
    }
  }
  return list;
}

List InfixToPostfix(List infix) {
  List<String> operations = ['+', '-', '*', '/', '^'];
  List<String> operators = [];
  List<String> postfix = [];
  List<String> checkOperators = ['+', '-', '*', '/', '^'];
  Map<String, int> valueOperators = {
    '+': 1,
    '-': 1,
    '*': 2,
    '/': 2,
    '^': 3,
    '(': 4,
    ')': 4
  };

  for (int i = 0; i < infix.length; i++) {
    if (isInteger(infix[i])) {
      postfix.add(infix[i]);
    }

    if (operations.contains(infix[i])) {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          valueOperators[infix[i]]! < valueOperators[infix.last]!) {
        postfix.add(operators.removeLast());
      }
      operators.add(infix[i]);
    }

    if (infix[i] == '(') {
      operators.add(infix[i]);
    }

    if (infix[i] == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }

      operators.removeLast();
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }

  return postfix;
}

List evaluatePostfix(List postfix) {
  List<num> values = [];
  var right;
  var left;
  var result;
  for (var p in postfix) {
    for (int i = 0; i < postfix.length; i++) {
      if (isInteger(postfix[i])) {
        values.add(postfix[i]);
      } else {
        right = values.removeLast();
        left = values.removeLast();

        if (p == '+') {
          result = left + right;
          values.add(result);
        } else if (p == '-') {
          result = left - right;
          values.add(result);
        } else if (p == '*') {
          result = left * right;
          values.add(result);
        } else if (p == '/') {
          result = left / right;
          values.add(result);
        } else if (p == '^') {
          result = pow(left, right);
          values.add(result);
        }
      }
    }
  }

  return values;
}

bool isInteger(String s) {
  for (int i = 0; i < 10; i++) {
    if (s[0] == i.toString()) {
      return true;
    }
  }
  return false;
}

