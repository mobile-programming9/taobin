import 'dart:io';

void main() {
  // int sweet = 0;
  double total = 0;
  print("Touch To Start Taobin.");

  List<Map<String, int>> product = [
    {'HOT BROWNSUGAR LATTE ': 40},
    {'HOT MILK ': 30},
    {'HOT THAI MILK TEA ': 35},
    {'ICED ESPRESSO ': 45},
    {'ICED COCOA ': 35},
    {'ICED TAIWANESE TEA ': 40},
    {'CARAMEL PROTEIN SHAKE ': 55},
    {'ICED STRAWBERRY ': 35},
    {'ICED LIME SALA SODA ': 25}
  ];

  List<String> sweetlevel = [
    'No Sugar Added',
    'Little Sweet',
    'Sweet Fit',
    'So Sweet',
    'Sweet 3 Worlds'
  ];

  List<String> payment = [
    'Cash',
    'Scan QRCode',
    'Use Taobin Credit',
    'Use Coupon/View Coupon'
  ];

  int menu, level, pay;

  while (true) {
    for (int i = 0; i < product.length; i++) {
      print("${i + 1}.${product[i].keys.first} ${product[i].values.first}");
    }
    menu = int.parse(stdin.readLineSync()!);
    if (menu > product.length) {
      print("Pleast Try Again");
    } else {
      break;
    }
  }

  while (true) {
    for (int i = 0; i < sweetlevel.length; i++) {
      print("${i + 1}. ${sweetlevel[i]}");
    }
    level = int.parse(stdin.readLineSync()!);
    if (level > sweetlevel.length) {
      print("Please Try Again");
    } else {
      break;
    }
  }

  while (true) {
    for (int i = 0; i < payment.length; i++) {
      print("${i + 1}. ${payment[i]}");
    }
    pay = int.parse(stdin.readLineSync()!);
    if (pay > payment.length) {
      print("Please Try Again");
    } else {
      break;
    }
  }

  print("Please wait a moment for the product.");
  print("Thank you for using Taobin");
}
